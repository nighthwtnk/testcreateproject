//
//  ViewController.swift
//  TestProject
//
//  Created by Nighthwtnk on 4/19/21.
//

import UIKit
import Alamofire
import SwiftyJSON

class MemberViewController: UIViewController {

    var members: [Member]?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpComponent()
        setUpDate()
    }
    func setUpComponent(){
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    func setUpDate(){
        let urlFile = "http://bnk48-public.dev.bnk48.io/members/all"
        Alamofire.request(urlFile, method: .get).responseJSON{ (response) in
            switch response.result
            {
            case.success(let value):
                //print(response.result)
                //let memberList = try? JSON(data:response.data!)
                //print(memberList![])
                let data = JSON(value)
                data.array?.forEach({ (member) in
                    print(data)
                    let newMember = Member(json:JSON(Member.self))
   
                    self.tableView.reloadData()
                    self.members?.append(newMember)

                })
                break

            case.failure:
                print("Error JSON")
            }
        }
    }
}

extension ViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.members?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = self.members?[indexPath.row].codeName
        return cell
    }
}

