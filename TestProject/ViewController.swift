//
//  ViewController.swift
//  TestProject
//
//  Created by Nighthwtnk on 4/19/21.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON

class ViewController: UIViewController{
    private var members: [Member] = []

    @IBOutlet var tableMemberList : UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Alamofire.request("http://bnk48-public.dev.bnk48.io/members/all").responseJSON{ (response) in
            switch response.result {
            case.success(_):
                let json = try! JSON(data:response.data!)
                let allMember = JSON(json).arrayValue.map{Member(json: $0)}
                self.members = allMember
                
                self.tableMemberList.reloadData()
                break
            case.failure:
                print("Error JSON")
            }
        }
    }
}

extension ViewController: UITabBarDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableMemberList.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! TableViewCell
        let name = self.members[indexPath.row].codeName?.uppercased()
        let fullName = self.members[indexPath.row].subtitleEn?.localizedCapitalized
        let urlImage = self.members[indexPath.row].profileImageUrl! as String
        Alamofire.request(urlImage).responseImage{(response) in
            if let image = response.result.value {
                DispatchQueue.main.async {
                    cell.imageProfile.image = image
                }
            }
        }
        cell.labelName.text = name
        cell.labelFullName.text = fullName
        
        return cell
    }
}





    
    




