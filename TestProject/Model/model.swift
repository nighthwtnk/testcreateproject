//
//  model.swift
//  TestProject
//
//  Created by Nighthwtnk on 4/19/21.
//

import Foundation
import SwiftyJSON

struct Member{
    let id: Int?
        let codeName: String?
        let displayName: String?
        let displayNameEn: String?
        let subtitle: String?
        let subtitleEn: String?
        let profileImageUrl: String?
        let coverImageUrl: String?
        let caption: String?
        let city: String?
        let cityEn: String?
        let country: String?
        let countryEn: String?
        let brand: String?
        let hashtags: [String]?
        let birthdate: String?
        let graduatedAt: Any?
    
    init(json: JSON!) {
        id = json["id"].intValue
        codeName = json["codeName"].stringValue
        displayName = json["displayName"].stringValue
        displayNameEn = json["displayNameEn"].stringValue
        subtitle = json["subtitle"].stringValue
        subtitleEn = json["subtitleEn"].stringValue
        profileImageUrl = json["profileImageUrl"].stringValue
        coverImageUrl = json["coverImageUrl"].stringValue
        caption = json["caption"].stringValue
        city = json["city"].stringValue
        cityEn = json["cityEn"].stringValue
        country = json["country"].stringValue
        countryEn = json["countryEn"].stringValue
        brand = json["brand"].stringValue
        hashtags = json["hashtags"].arrayValue.map { $0.stringValue }
        birthdate = json["birthdate"].stringValue
        graduatedAt = json["graduatedAt"]

    }
}
